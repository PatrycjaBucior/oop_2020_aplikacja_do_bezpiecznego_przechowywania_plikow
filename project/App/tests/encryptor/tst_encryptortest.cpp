#include <QtTest>
#include "../../application/interface/encryptor.h"

// add necessary includes here

class encryptorTest : public QObject
{
    Q_OBJECT

public:
    encryptorTest();
    ~encryptorTest();

private slots:
    void encryptAndDecryptTest();

};

encryptorTest::encryptorTest()
{

}

encryptorTest::~encryptorTest()
{

}

void encryptorTest::encryptAndDecryptTest()
{
    Encryptor enc{};
    QString exmpl_str = "Example text to encrypt and decrypt";
    QString encr_str = enc.encrypt(exmpl_str);
    QString decr_str = enc.decrypt(encr_str);

    QCOMPARE(exmpl_str, decr_str);
}

QTEST_APPLESS_MAIN(encryptorTest)

#include "tst_encryptortest.moc"
