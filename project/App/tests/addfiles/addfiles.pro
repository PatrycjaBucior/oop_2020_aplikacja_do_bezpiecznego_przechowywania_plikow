QT += testlib
QT += gui
QT += sql
QT += widgets
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

HEADERS += \
    ../../application/interface/dbsetup.h \
    ../../application/interface/addfiles.h \
    ../../application/interface/encryptor.h \
    ../../application/interface/filesfactory.h \

SOURCES +=  tst_addfilestest.cpp \
    ../../application/interface/dbsetup.cpp \
    ../../application/interface/addfiles.cpp \
    ../../application/interface/encryptor.cpp \
    ../../application/interface/filesfactory.cpp \
