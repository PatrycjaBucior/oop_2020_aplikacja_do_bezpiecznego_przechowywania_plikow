#include <QtTest>
#include <QCoreApplication>
#include <QSql>

#include "../../application/interface/dbsetup.h"
#include "../../application/interface/addfiles.h"
#include "../../application/interface/encryptor.h"
#include "../../application/interface/filesfactory.h"

// add necessary includes here

class AddFilesTest : public QObject
{
    Q_OBJECT

public:
    AddFilesTest();
    ~AddFilesTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void AddRegisteredUserFileTest();
    void AddFakeUserFilesTest();
    void DeleteRegiteredUserFilesTest();
    void DeleteFakeUserFilesTest();

};

AddFilesTest::AddFilesTest()
{

}

AddFilesTest::~AddFilesTest()
{

}

void AddFilesTest::initTestCase()
{

}

void AddFilesTest::cleanupTestCase()
{

}

void AddFilesTest::AddRegisteredUserFileTest()
{
    DbSetup database{};
    database.CreateConnection();
    QSqlDatabase::database();
    QSqlQuery qry;
    qry.exec("insert into users (login, password) values ('test1','12345')");
    AddFiles addfiles;
    addfiles.AddRegisteredUserFile("aaa", "bbb",1, "txt");
    QSqlQueryModel model;
    model.setQuery("SELECT id FROM files WHERE user_id=1 AND name='aaa' AND type='.txt'");
    int id = model.record(0).value("id").toInt();
    QCOMPARE(1, id);
}
void AddFilesTest::AddFakeUserFilesTest()
{
    DbSetup database{};
    database.CreateConnection();
    QSqlDatabase::database();
    AddFiles addfiles;
    QString dirPath = QDir::currentPath() + "/user";
        FilesFactory filesfactory;
        filesfactory.clearUserDir();
        filesfactory.createUserDir();
    addfiles.AddFakeUserFile("aaa", "bbb","txt");
    QFileInfo check_file(dirPath + "/aaa.txt" );
    QCOMPARE(1, check_file.exists());
}

void AddFilesTest::DeleteRegiteredUserFilesTest()
{
    DbSetup database{};
    database.CreateConnection();
    QSqlDatabase::database();
    AddFiles addfiles;
    addfiles.DeleteRegisteredUserFile(1, "aaa" ,"txt");
    QSqlQueryModel model;
    model.setQuery("SELECT id FROM files WHERE user_id=1 AND name='aaa' AND type='.txt'");
    int id = model.record(0).value("id").toInt();
    QCOMPARE(0, id);
}

void AddFilesTest::DeleteFakeUserFilesTest()
{
    DbSetup database{};
    database.CreateConnection();
    QSqlDatabase::database();
    AddFiles addfiles;
    QString dirPath = QDir::currentPath() + "/user";
        FilesFactory filesfactory;
        filesfactory.clearUserDir();
        filesfactory.createUserDir();
    addfiles.DeleteFakeUserFile("aaa", "txt");
    QFileInfo check_file(dirPath + "/aaa.txt" );
    QCOMPARE(0, check_file.exists());
}
QTEST_MAIN(AddFilesTest)

#include "tst_addfilestest.moc"
