#include <QtTest>
#include <QObject>
#include <QFile>
#include "../../application/interface/dbsetup.h"

// add necessary includes here

class dbsetupTest : public QObject
{
    Q_OBJECT

public:
    dbsetupTest();
    ~dbsetupTest();

private slots:
    void databaseCreationTest();
    void databaseConnectionTest();

};

dbsetupTest::dbsetupTest()
{

}

dbsetupTest::~dbsetupTest()
{

}

void dbsetupTest::databaseCreationTest()
{
    DbSetup database{};
    QDir currDir = QDir::current();
    currDir.cdUp();
    QFileInfo check_file(currDir.path() + "/dbsetupsqlite.db" );
    QCOMPARE(check_file.exists(),1);
}

void dbsetupTest::databaseConnectionTest()
{
     DbSetup database{};
     int x = database.CreateConnection();
     QCOMPARE(x, 1);
}

QTEST_APPLESS_MAIN(dbsetupTest)

#include "tst_dbsetuptest.moc"
