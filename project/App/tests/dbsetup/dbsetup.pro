QT += testlib
QT -= gui
QT += sql

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    ../../application/interface/dbsetup.h \

SOURCES +=  tst_dbsetuptest.cpp \
    ../../application/interface/dbsetup.cpp \


