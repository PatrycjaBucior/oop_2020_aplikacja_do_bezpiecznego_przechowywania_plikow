#include <QtTest>
#include "../../application/interface/filesfactory.h"

// add necessary includes here

class filesfactory : public QObject
{
    Q_OBJECT

public:
    filesfactory();
    ~filesfactory();

private slots:
    void userDirTest();
    void createFileTest();
    void makeTrueUserFilesTest();
    void copyFakeUserFilesTest();

};

filesfactory::filesfactory()
{

}

filesfactory::~filesfactory()
{

}

void filesfactory::userDirTest()
{
    QString dirPath = QDir::currentPath() + "/user";
    QDir dir(dirPath);

    FilesFactory filesfactory;
    filesfactory.createUserDir(dirPath);

    QCOMPARE(1, dir.exists());

    filesfactory.clearUserDir(dirPath);

    QCOMPARE(0, dir.exists());
}

void filesfactory::createFileTest()
{
    QString dirPath = QDir::currentPath() + "/user";
    QDir dir(dirPath);

    FilesFactory filesfactory;
    filesfactory.clearUserDir(dirPath);
    filesfactory.createUserDir(dirPath);

    filesfactory.createFile("test", "test", ".txt", QDir::currentPath());
    QFile file(dirPath + "/test.txt");
    QCOMPARE(1, file.exists());

     filesfactory.clearUserDir(dirPath);
}

void filesfactory::makeTrueUserFilesTest()
{
    DbSetup database{};
    database.CreateConnection();
    QSqlQuery qry;
    qry.exec("insert into users (login, password) values ('test1','test1')");
    QSqlQueryModel model;
    model.setQuery("select id from users where login ='test1' and password ='test1'");
    int id = model.record(0).value("id").toInt();

    qry.prepare("INSERT INTO files (name, content, user_id, type) "
                  "VALUES (:name, :content, :user_id, :type)");
    qry.bindValue(":name", "test1");
    qry.bindValue(":content", "test1");
    qry.bindValue(":user_id", id);
    qry.bindValue(":type", ".txt");
    qry.exec();

    FilesFactory filesfactory;
    QString dirPath = QDir::currentPath() + "/user";
    QDir dir(dirPath);
    filesfactory.createUserDir(dirPath);
    filesfactory.makeTrueUserFiles(id);
    QFileInfo check_file(dirPath + "/test1.txt");

    QCOMPARE(1, check_file.exists());

    filesfactory.clearUserDir(dirPath);
}

void filesfactory::copyFakeUserFilesTest()
{
    FilesFactory filesfactory;
    QString dirPath = QDir::currentPath() + "/user";
    QDir dir(dirPath);
    filesfactory.createUserDir(dirPath);
    filesfactory.copyFakeFiles();

    QDir currDir = QDir::current();
    for(int i=0; i<4; ++i)
    {
        currDir.cdUp();
    }

    QString rootDir = currDir.path();

    QDir directory(rootDir);
    QStringList files = directory.entryList(QStringList(),QDir::Files);

    for(QString filename : files) {
        QFile file(dirPath + '/' + filename);
        QCOMPARE(1, file.exists());
    }
    filesfactory.clearUserDir(dirPath);
}


QTEST_APPLESS_MAIN(filesfactory)

#include "tst_filesfactory.moc"
