QT += testlib
QT -= gui
QT += sql
QT += widgets

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_filesfactory.cpp \
    ../../application/interface/filesfactory.cpp \
    ../../application/interface/dbsetup.cpp \
    ../../application/interface/encryptor.cpp

HEADERS += \
    ../../application/interface/filesfactory.h \
    ../../application/interface/dbsetup.h \
    ../../application/interface/encryptor.h





