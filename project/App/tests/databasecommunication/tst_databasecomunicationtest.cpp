#include <QtTest>
#include <QObject>
#include "../../application/interface/databasecommunication.h"
#include "../../application/interface/dbsetup.h"

// add necessary includes here

class databasecomunicationTest : public QObject
{
    Q_OBJECT

public:
    databasecomunicationTest();
    ~databasecomunicationTest();

private slots:
    void Hash_passwordTest();
    void Get_User_IdTest();

};

databasecomunicationTest::databasecomunicationTest()
{

}

databasecomunicationTest::~databasecomunicationTest()
{

}

void databasecomunicationTest::Hash_passwordTest()
{
    DatabaseCommunication dc{};
    QString password = "1";

    QCOMPARE(dc.Hash_Password(password), (QByteArray)("6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b"));

}

void databasecomunicationTest::Get_User_IdTest()
{
    DbSetup database{};
    database.CreateConnection();
    DatabaseCommunication dc{};
    QString username, password;
    username = "user";
    password = "password";
    QByteArray hash_password;
    hash_password = dc.Hash_Password(password);
    QSqlQuery qry;

    qry.exec("insert into users (login, password) values ('" + username + "','" + hash_password + "')");

    QSqlQueryModel model;
    model.setQuery("select id from users where login ='" + username + "' and password ='" + hash_password + "'");
    int id = model.record(0).value("id").toInt();

    QCOMPARE(dc.Get_User_Id(username,password),id);
}

QTEST_APPLESS_MAIN(databasecomunicationTest)

#include "tst_databasecomunicationtest.moc"
