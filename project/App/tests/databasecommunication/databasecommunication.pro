QT += testlib
QT -= gui
QT += sql

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_databasecomunicationtest.cpp \
    ../../application/interface/databasecommunication.cpp \
    ../../application/interface/dbsetup.cpp

HEADERS += \
    ../../application/interface/databasecommunication.h \
    ../../application/interface/dbsetup.h
