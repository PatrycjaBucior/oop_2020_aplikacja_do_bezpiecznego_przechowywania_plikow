#include "addfiles.h"


AddFiles::AddFiles(QWidget *parent) : QWidget(parent)
{

}

void AddFiles::AddToDatabase(int userid, QString filename)
{
    QSqlDatabase::database();
    QFile file(filename);
    QString text = "";
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qDebug("error opening file");
            return;
    }

    else {
            Encryptor enc;
            QTextStream in(&file);
            text = in.readAll();
            text = enc.encrypt(text);
    }

    QFileInfo info(file);
    QString ext = info.suffix();
    QString name = info.baseName();

    if(userid == 0) {
        AddFakeUserFile(name, text, ext);
    }

    else {
        AddRegisteredUserFile(name, text, userid, ext);
    }
}


int AddFiles::AddRegisteredUserFile(QString name, QString text, int usr_id, QString extension)
{
    QSqlDatabase::database();
    QSqlQuery query;

        query.prepare("INSERT INTO files (name, content, user_id, type) "
                      "VALUES (:name, :content, :user_id, :type)");
        query.bindValue(":name", name);
        query.bindValue(":content", text);
        query.bindValue(":user_id", usr_id);
        query.bindValue(":type", '.' + extension);
        query.exec();
        return 1;
}

int AddFiles::AddFakeUserFile(QString name, QString text, QString extension)
{
    QFile file(QDir::currentPath() + "/user/" + name + '.' + extension);
    if (!file.open(QIODevice::ReadWrite))
    {
        return 0;
    }
    QTextStream out(&file);
    out << text;
    file.close();

    return 1;
}

void AddFiles::DeleteRegisteredUserFile(int usr_id, QString filename, QString ext)
{
    QSqlDatabase::database();
    QSqlQuery query;

    int ok = query.prepare("DELETE FROM files WHERE user_id=:usr_id AND name=:filename AND type=:ext");
    if(!ok)
    {
        qDebug() << "Error preparing query!\n";
    }
    query.bindValue(":usr_id",QString::number(usr_id));
    query.bindValue(":filename", filename);
    query.bindValue(":ext",'.' + ext);
    query.exec();

}

void AddFiles::DeleteFakeUserFile(QString filename, QString ext)
{
    QFile file(QDir::currentPath() + "/user/" + filename + '.' + ext);
    file.remove();
}
