#ifndef DATABASECOMMUNICATION_H
#define DATABASECOMMUNICATION_H

#include <QObject>
#include "dbsetup.h"
#include <cstring>
#include <string>

class DatabaseCommunication : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseCommunication(QObject *parent = nullptr);
    int Get_User_Id(QString login, QString password);
    QByteArray Hash_Password(QString string);


signals:

};

#endif // DATABASECOMMUNICATION_H
