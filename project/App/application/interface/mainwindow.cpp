#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label_name->setVisible(false);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_login_clicked()
{
     QSqlDatabase::database();
     QString login, password;
     login = ui->lineEdit_login->text();
     password = ui->lineEdit_password->text();

     QByteArray hash_password;
     DatabaseCommunication dc;

     hash_password = dc.Hash_Password(password);

     FilesFactory factory{};
     factory.clearUserDir();
     factory.createUserDir();

     QSqlQuery qry;

     if(qry.exec("select * from users where login ='" + login + "' and password ='" + hash_password + "'"))
     {
         int count = 0;
         while(qry.next())
         {
            count++;
         }
         if(count == 1)
         {
            factory.makeTrueUserFiles(dc.Get_User_Id(login, password));
         }
         else
         {
             factory.copyFakeFiles();
         }
     }
     createFileExplorer();
     emit credentials(login, password);
}


void MainWindow::on_pushButton_signin_clicked()
{
    QSqlDatabase::database();

    QString username = ui->lineEdit_username_2->text();
    QString new_password = ui->lineEdit_password_2->text();

    QByteArray hash_password;
    DatabaseCommunication dc;

    hash_password= dc.Hash_Password(new_password);

    QSqlQuery qry;

    int count2 = 0;
    if(qry.exec("select * from users where login ='" + username + "'"))
   {
        while(qry.next())
        {
           count2++;
        }

        if(count2==1)
        {
            ui->label_name->setText("<font color='red'>User with this username already exists!</font>");
            ui->label_name->setVisible(true);

        }
        if(count2<1)
        {
            if(qry.exec("insert into users (login, password) values ('" + username + "','" + hash_password + "')"))
            {
                //action after registered
                ui->label_name->setText("<font color='green'>You are registered successfully!</font>");
                ui->label_name->setVisible(true);
                ui->pushButton_signin->setDisabled(true);
            }
        }
    }
}


void MainWindow::createFileExplorer()
{
    dialogCorrect = new DialogCorrect(this);
    QObject::connect(this, SIGNAL(credentials(QString, QString)), dialogCorrect, SLOT(getCredentials(QString, QString)));
    QObject::connect(dialogCorrect, SIGNAL(logout()), this, SLOT(loggedout()));
    hide();
    dialogCorrect->show();
}


void MainWindow::loggedout()
{
    ui->lineEdit_login->setText("");
    ui->lineEdit_password->setText("");
    show();
}
