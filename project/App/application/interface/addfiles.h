#ifndef ADDFILES_H
#define ADDFILES_H

#include <QWidget>
#include <QFile>
#include <QFileDialog>
#include <QSqlDatabase>
#include <QDateTime>
#include <QSqlQuery>
#include <QTextStream>
#include <QDebug>
#include <QDir>
#include "encryptor.h"

class AddFiles : public QWidget
{
    Q_OBJECT
public:
    explicit AddFiles(QWidget *parent = nullptr);
    void AddToDatabase(int userid, QString filename);
    int AddRegisteredUserFile(QString name, QString text, int usr_id, QString extension);
    int AddFakeUserFile(QString name, QString text, QString extension);
    void DeleteRegisteredUserFile(int usr_id, QString filename, QString ext);
    void DeleteFakeUserFile(QString filename, QString ext);
signals:

};

#endif // ADDFILES_H
