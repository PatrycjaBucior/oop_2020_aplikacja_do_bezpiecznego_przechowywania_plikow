#include "viewfile.h"


ViewFile::ViewFile(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewFile)
{
    ui->setupUi(this);
}

ViewFile::~ViewFile()
{
    delete ui;
}

void ViewFile::view_file(QString file_path)
{
    QFile file(file_path);

    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        Encryptor enc;
        QTextStream in(&file);
        QString filecontent = in.readAll();
        filecontent=enc.decrypt(filecontent);
        ui->textBrowser->setText(filecontent);
    }

}
