#include "mainwindow.h"
#include "dbsetup.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    DbSetup database{};
    database.CreateConnection();
    w.show();
    return a.exec();
}




