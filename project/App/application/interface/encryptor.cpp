#include "encryptor.h"


Encryptor::Encryptor(QObject *parent): QObject(parent){};

QString Encryptor::encrypt(QString str2encrypt)
{
    char ch;
    std::string encrypted;
    for (int i = 0; i < str2encrypt.size(); ++i) {
        ch = char(str2encrypt.at(i).toLatin1()-7);
        encrypted += ch;
    }
     return QString::fromStdString(encrypted);
}

QString Encryptor::decrypt(QString str2decrypt)
{
    char ch;
    std::string decrypted;
    for (int i = 0; i < str2decrypt.size(); ++i) {
        ch = char(str2decrypt.at(i).toLatin1()+7);
        decrypted += ch;
    }
     return QString::fromStdString(decrypted);
}
