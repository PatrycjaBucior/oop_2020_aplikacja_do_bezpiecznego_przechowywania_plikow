#ifndef DBSETUP_H
#define DBSETUP_H

#include <QObject>
#include <QtSql>
#include <QDir>

class DbSetup : public QObject
{
    Q_OBJECT
public:
    QSqlDatabase db;
    explicit DbSetup(QObject *parent = nullptr);
    bool CreateConnection();

signals:

};

#endif // DBSETUP_H
